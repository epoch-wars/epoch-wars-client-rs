# epoch-wars-client-rs
Simple client for epoch-wars.

## Installation

 * Install [Rust](https://rustup.rs)
 * Start with `cargo run --release`
